/*
// the following should be sourced from config.js before the index.js is loaded
const config = {
  words: [
    { full: "Ernte", sounds: [ "E", "r", "n", "t", "e" ] },
    { full: "Arbeit", sounds: [ "A", "r", "b", "ei", "t" ] },
    // ... and so on ...
  ],
  title: "Wort hören und schreiben",
  description: "Höre dir das Wort und die einzelnen Silben an und schreibe sie in die richtigen Felder.",
  next: "#this-link-does-not-lead-anywhere-yet",
  nextLabel: "Zur nächsten Übung",
  audioSource: "../audio/"
}
*/

// set config and $ as global vars for the linter
/* global config */
/* global $ */

// the following two variables are to be compatible with the final WP plugin implementation
const DigMitConfig = config
const digMitExerciseID = 1

class ListenAndWrite {
  constructor (id, config) {
    this.config = config
    this.id = id
    this.$container = $('#digmit-exercise-container-' + id)
  }

  init () {
    this.initialiseContainer()
    this.initialiseHeader()
    this.initialiseWord(this)
    $('#next-word-button').on('click', { self: this }, this.prepareNextWord)
    $('#next-word-button span').text(this.config.nextWordLabel)
    $('#next-exercise-button span').text(this.config.nextLabel)
    $('#next-exercise-button').on('click', () => {
      window.location = this.config.next
    })
  }

  initialiseContainer () {
    $('<div class="header"><h2></h2></div>').appendTo(this.$container)
    $('<div class="description"></div>').appendTo(this.$container)

    const successMessage = `
      <div class="success-message" style="display: none">
        <p>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          Super!
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
        </p>
        <div id="next-exercise-button">
          <span></span>
          <i class="fa fa-arrow-circle-right"></i>
        </div>
      </div>
    `
    $(successMessage).appendTo(this.$container)

    const nextWordMessage = `
      <div class="next-word-message" style="display: none">
        <p>
          <i class="fa fa-star fa-spin"></i>
          Super!
          <i class="fa fa-star fa-spin"></i>
        </p>
        <div id="next-word-button">
          <span></span>
          <i class="fa fa-arrow-circle-right"></i>
        </div>
      </div>
    `
    $(nextWordMessage).appendTo(this.$container)

    $('<div class="target-field single-target-area height6"></div>').appendTo(this.$container)
  }

  initialiseHeader () {
    const config = this.config
    $('title').text(config.title)
    $('.header h2').text(config.title)
    $('.description').text(config.description)
  }

  // take the next word from config.words array, create elements for the
  // audio buttons and the inputs and place them in the target field
  initialiseWord (self) {
    const config = self.config
    // check if this is the frist word in the exercise and create counter
    // otherwise increase counter for the next word
    if (config.counter === undefined) {
      config.counter = 0
    } else {
      config.counter++
    }

    const word = config.words[config.counter]
    // if it is configured, we add a button to listen to the full word
    if (config.showFullWord) {
      const $wordButton = self.createFullWord(self, word.full)
      $wordButton.appendTo($('.target-field'))
    }
    // now add the input boxes with the input elements
    let char = 0
    for (const i in word.sounds) {
      const $div = $('<div class="input-box"></div>')
      // we only need one input field for sounds consisting of one character
      if (word.sounds[i].length === 1) {
        $('<input type="text" maxlength="1" id="exercise-input-' + char + '">')
          .appendTo($div)
          .on('input', { position: char, self }, self.processInput)
          .on('focus', { label: word.sounds[i].toLowerCase(), source: config.audioSource }, self.playAudio)
          .on('focus click', event => $(this).select())
          .on('mouseup', event => event.preventDefault())
        char++
      } else {
      // for sounds with more characters we add more input elements
        for (const j in word.sounds[i]) {
          $('<input type="text" maxlength="1" id="exercise-input-' + char + '">')
            .appendTo($div)
            .on('input', { position: char, self }, self.processInput)
            .on('focus', { label: word.sounds[i][j].toLowerCase(), source: config.audioSource }, self.playAudio)
            .on('focus click', event => $(this).select())
            .on('mouseup', event => event.preventDefault())
          char++
        }
      }
      // add an audio button to listen to the sound
      $('<br>').appendTo($div)
      $('<i class="fa fa-volume-up"></i>').appendTo($div)
        .on('click', { label: word.sounds[i].toLowerCase(), source: config.audioSource }, self.playAudio)
      // add an info symbol with a solution
      $('<br>').appendTo($div)
      $('<i class="help fa fa-question-circle"></i>').appendTo($div)
        .on('click', event => {
          $('#solution-' + i).toggle('fade')
        })
      $('<div class="solution"><span id="solution-' + i + '">' + word.sounds[i] + '</span></div>').appendTo($div)
      // now append the whole div to the exercise target area
      $div.appendTo($('.target-field'))
    }
  }

  // play an audio file associated with a word
  playAudio (event) {
    const source = event.data.source
    const label = event.data.label
    const audio = new Audio(source + label + '.mp3')
    audio.play()
  }

  // creates and returns a clickable and non-draggable button with a text
  // that is played back when the user clicks on it
  createFullWord (self, text) {
    const el = '<div class="full-word"><i class="fa fa-volume-up"></i></div>'
    const $el = $(el)
    $el.on('click', { label: text, source: self.config.audioSource }, self.playAudio)
    return $el
  }

  processInput (event) {
    const self = event.data.self
    const config = self.config
    const position = event.data.position
    const value = event.originalEvent.data
    const word = config.words[config.counter]
    const $input = $(event.target)

    // check if the new character fits the position and apply CSS classes
    if (value === null) {
      $input.removeClass('correct incorrect')
    } else {
      // the answer should be correct even if the case does not match
      if (value.toLowerCase() === word.full[position].toLowerCase()) {
        // but we want to fill in the correct case anyway
        $input.val(word.full[position])
        $input.addClass('correct').removeClass('incorrect')
        // if the input character was correct and we are not in the last field
        // then switch the focus to the next character
        if (position < word.full.length - 1) {
          $('#exercise-input-' + (position + 1)).focus()
        }
      } else {
        $input.addClass('incorrect').removeClass('correct')
      }
    }

    // check if all items are already placed correctly and set visibility for
    // the next word or success message accordingly
    if (self.allCorrect(word)) {
      // deactivate all input elements
      $('.input-box input').attr('disabled', 'true')
      // if the audio button to listen to the full word was not shown so far, add it now
      if (!config.showFullWord) {
        const $wordButton = self.createFullWord(self, word.full)
        $wordButton.prependTo($('.target-field'))
      }
      // now check if this was already the last word in the exercise
      if (config.counter + 1 >= config.words.length) {
        $('.success-message').css('display', 'block')
      } else {
        $('.next-word-message').css('display', 'block')
      }
    } else {
      $('.success-message').css('display', 'none')
    }
  }

  // check if all fields have been filled correctly and return true or false
  allCorrect (word) {
    for (let i = 0; i < word.full.length; i++) {
      const value = $('#exercise-input-' + i).val()
      if (value === '' || value !== word.full[i]) {
        return false
      }
    }
    return true
  }

  // this is called after a word was solved to prepare for the next word in the
  // exercise. we have to restore the target field accordingly
  prepareNextWord (event) {
    const self = event.data.self
    $('.target-field').children().remove()
    $('.next-word-message').css('display', 'none')
    self.initialiseWord(self)
  }
}

// start initialisation as soon as the document is ready
$('document').ready(function () {
  const exercise = new ListenAndWrite(digMitExerciseID, DigMitConfig)
  exercise.init()
})
