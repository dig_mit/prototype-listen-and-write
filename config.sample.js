/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "config" }] */

// config object with list of available words for this exercise
// in the integrated version this will be generated dynamically by the backend
const config = {
  words: [
    { full: 'Ernte', sounds: ['E', 'r', 'n', 't', 'e'] },
    { full: 'Arbeit', sounds: ['A', 'r', 'b', 'ei', 't'] },
    { full: 'schwer', sounds: ['sch', 'w', 'e', 'r'] },
    { full: 'Tage', sounds: ['T', 'a', 'g', 'e'] },
    { full: 'Woche', sounds: ['W', 'o', 'ch', 'e'] },
    { full: 'Uhr', sounds: ['U', 'h', 'r'] },
    { full: 'Früh', sounds: ['F', 'r', 'ü', 'h'] },
    { full: 'Euro', sounds: ['Eu', 'r', 'o'] },
    { full: 'Stunde', sounds: ['St', 'u', 'n', 'd', 'e'] },
    { full: 'Lohn', sounds: ['L', 'o', 'h', 'n'] },
    { full: 'Rechte', sounds: ['R', 'e', 'ch', 't', 'e'] },
    { full: 'Chef', sounds: ['Ch', 'e', 'f'] },
    { full: 'Gericht', sounds: ['G', 'e', 'r', 'i', 'ch', 't'] },
  ],
  // the title / header of the exercise
  title: 'Wort hören und schreiben',
  // a sentence or short paragraph to describe the exercise
  description: 'Höre dir das Wort und die einzelnen Silben an und schreibe sie in die richtigen Felder.',
  // if an audio button to hear the full word should be shown
  showFullWord: true,
  // link to the next exercise / page
  next: '#this-link-does-not-lead-anywhere-yet',
  // labels that are shown as links to the next word / exercise (or page)
  nextWordLabel: 'Nächstes Wort',
  nextLabel: 'Nächste Übung',
  // directory where audio files for words and syllables are stored
  audioSource: '../audio/',
}
